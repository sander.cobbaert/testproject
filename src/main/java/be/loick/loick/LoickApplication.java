package be.loick.loick;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import javax.persistence.Entity;
import javax.persistence.Id;

@SpringBootApplication
public class LoickApplication {

    @Autowired
    private BookRepository bookRepository;

    public static void main(String[] args) {
        SpringApplication.run(LoickApplication.class, args);
    }


    @Bean
    public ApplicationRunner applicationRunner() {
        return a -> {
            Book book = new Book("aBook");
            bookRepository.save(book);

            Iterable<Book> all = bookRepository.findAll();
            System.out.println(all.iterator().hasNext());
        };
    }

}
